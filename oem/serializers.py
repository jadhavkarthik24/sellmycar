from rest_framework import serializers
from .models import Specs


class SpecsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Specs
        fields = '__all__'