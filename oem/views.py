from .serializers import SpecsSerializer
from .models import Specs
from rest_framework.response import Response
from rest_framework import filters
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated, AllowAny


class OemModelView(generics.ListAPIView):
    """ return list of oem models availabe and lets user search for model name(honda city) and year"""
    permission_classes = [AllowAny, ]

    queryset = Specs.objects.all()
    serializer_class = SpecsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['model_name', 'year']
