from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.


def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)


class Specs(models.Model):
    model_name = models.CharField(max_length=128)
    year = models.SmallIntegerField(
        validators=[MinValueValidator(1991), max_value_current_year])
    list_price = models.IntegerField()
    color = models.CharField(max_length=64)
    mileage = models.SmallIntegerField()
    power = models.PositiveIntegerField()
    max_speed = models.SmallIntegerField()

    def __str__(self):
        return self.model_name

    class Meta:
        ordering = ('-year',)
