from rest_framework import serializers
from oem.serializers import SpecsSerializer
from .models import MarketplaceInventory, User


class CurrentUserSerializer(serializers.Serializer):
    class Meta:
        model = User
        fields = '__all__'


class MarketplaceInventorySerializer(serializers.ModelSerializer):
    model_name = serializers.ReadOnlyField(source='oem_model.model_name')
    year = serializers.ReadOnlyField(source='oem_model.year')

    class Meta:
        model = MarketplaceInventory
        fields = ['id', 'model_name', 'year', 'odometer_km', 'major_scratches', 'original_paint',
                  'number_of_accidents', 'number_of_previous_buyers', 'registration_place']


class MIEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketplaceInventory
        fields = ['odometer_km', 'major_scratches', 'original_paint',
                  'number_of_accidents', 'number_of_previous_buyers', 'registration_place']


class AddMISerializer(serializers.ModelSerializer):

    class Meta:
        model = MarketplaceInventory
        fields = ['oem_model', 'odometer_km', 'major_scratches', 'original_paint',
                  'number_of_accidents', 'number_of_previous_buyers', 'registration_place', 'dealer']

    def to_representation(self, instance):
        self.fields['oem_model'] = SpecsSerializer(
            read_only=True)
        self.fields['dealer'] = CurrentUserSerializer(
            read_only=True)
        return super(AddMISerializer, self).to_representation(instance)
