from django.urls import path
from .views import *

urlpatterns = [
    path('inventories/', MarketplaceInventoryView.as_view(),
         name="inventories_listing"),
    path('edit/inventory/<int:pk>/', EditMIView.as_view(),
         name="edit_inventory"),
    path('delete/inventory/<int:pk>/', DeleteMIView.as_view(),
         name="delete_inventory"),
    path('add/inventory/', AddMIView.as_view(),
         name="add_inventory"),
]
