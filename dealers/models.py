from django.db import models
from oem.models import Specs
from django.contrib.auth.models import User


class MarketplaceInventory(models.Model):
    oem_model = models.ForeignKey(
        Specs, related_name='specs', on_delete=models.CASCADE)
    odometer_km = models.IntegerField()
    major_scratches = models.BooleanField()
    original_paint = models.BooleanField()
    number_of_accidents = models.SmallIntegerField()
    number_of_previous_buyers = models.SmallIntegerField()
    registration_place = models.CharField(max_length=128)
    dealer = models.ForeignKey(
        User, related_name='users', on_delete=models.CASCADE)

    class Meta:
        ordering = ('-number_of_accidents',)
