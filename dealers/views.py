from .serializers import MarketplaceInventorySerializer, MIEditSerializer,  AddMISerializer
from .models import MarketplaceInventory
from rest_framework.response import Response
from rest_framework import status
from rest_framework import filters
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated, AllowAny


class MarketplaceInventoryView(generics.ListAPIView):
    """ return list of added inventories availabe and lets user search for model name(honda city) and year"""
    # Disables default pagination
    permission_classes = [IsAuthenticated, ]

    serializer_class = MarketplaceInventorySerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['oem_model__model_name', 'oem_model__year']

    def get_queryset(self):
        queryset = MarketplaceInventory.objects.filter(
            dealer_id=self.request.user.id)
        return queryset


class EditMIView(generics.UpdateAPIView):
    """This API used for editing a inventory"""
    permission_classes = [IsAuthenticated, ]
    queryset = MarketplaceInventory.objects.all()
    serializer_class = MIEditSerializer


class DeleteMIView(generics.RetrieveDestroyAPIView):
    """ API is used for deleting a inventory"""
    permission_classes = [IsAuthenticated, ]
    queryset = MarketplaceInventory.objects.all()
    serializer_class = MIEditSerializer


class AddMIView(generics.ListCreateAPIView):
    """ API is used to add a new inventory for a given model"""
    permission_classes = [IsAuthenticated, ]
    queryset = MarketplaceInventory.objects.all()
    serializer_class = AddMISerializer

    def post(self, request, fromat=None):
        user_id = request.user.id
        request.data['dealer'] = user_id
        serializer = AddMISerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message": "Success"}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
