from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
    openapi.Info(
        title="Sell My Car API's Documentation",
        default_version='v1',
        description="SMC Corp is a marketplace for second hand cars. Dealers list their inventory of second hand cars on sellmycar.com. Buyers can look at the inventory and buy the car from sellmycar.com",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)
